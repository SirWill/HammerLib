package com.zeitheron.hammercore.api.multipart;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;

/**
 * {@link IHandlerProvider} for {@link MultipartSignature}
 */
public interface IMultipartHandlerProvider extends IHandlerProvider
{
}