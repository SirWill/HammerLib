package com.zeitheron.hammercore.api.tile;

/**
 * Marks given tile as 'pass-slip' - the tile is going to update even if it's tick rate was decreased.
 */
public interface ITickSlipPreventTile
{
}