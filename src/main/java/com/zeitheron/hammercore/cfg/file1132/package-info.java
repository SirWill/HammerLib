/**
 * This configuration package was back-ported from HammerLib 1.13.2 due to the
 * easiness and possibilities given by this format.
 */
package com.zeitheron.hammercore.cfg.file1132;