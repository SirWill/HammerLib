package com.zeitheron.hammercore.utils;

public interface ICopyable<T>
{
	T copy();
}