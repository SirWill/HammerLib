package com.zeitheron.hammercore.utils.java.itf;

public interface IArgumentFunction
{
	Object call(Object... args);
}